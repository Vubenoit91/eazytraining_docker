FROM ubuntu:18.04
MAINTAINER vubenoit (vbenoit91@live.com)
RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y nginx git
EXPOSE 80
#ADD static-website-example /var/www/html/
RUN rm -rf /var/www/html/
RUN git clone https://gitlab.com/Vubenoit91/eazytraining_docker.git /var/www/html/
ENTRYPOINT ["/usr/sbin/nginx", "-g", "daemon off;"]
